﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication3
{
    static class ourConverter
    {
        static private double MileToМetres = 16093;
        static private double SeaMileToMetres = 1852;
        static private double FeetToMetres = 0.3048;
        static private double InchToMetres = 0.0254;
        static public double convToMertes(double value)
        {
            double Metres = double.NaN;
            bool validator = true;
            Console.WriteLine("Какую величину вы хотите конвертировать в метры? 1-миля 2-морская миля 3- фут 4-дюйм");
            while (validator == true)
            switch (Console.ReadLine())
            {
                case "1":
                    {
                        Console.WriteLine("Введите кол-во миль");
                        Metres = value * MileToМetres;
                        Console.WriteLine("{0} метров ", Metres);
                        validator = false;
                        break;
                    }
                case "2":
                    {
                        Console.WriteLine("Введите кол-во морских миль");
                        Metres = value * SeaMileToMetres;
                        Console.WriteLine("{0} метров ", Metres);
                        validator = false;
                        break;
                    }
                case "3":
                    {
                        Console.WriteLine("Введите кол-во футов");
                        Metres = value * FeetToMetres;
                        Console.WriteLine("{0} метров ", Metres);
                        validator = false;
                        break;
                    }
                case "4":
                    {
                        Console.WriteLine("Введите кол-во дюймов");
                        Metres = value * InchToMetres;
                        Console.WriteLine("{0} метров ", Metres);
                        validator = false;
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Неправильный формат данных");
                        break;
                    }
            }
            return Metres;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            double value;
            while (true)
            {
                Console.WriteLine("Введите конвертируемое значение");
                if (double.TryParse(Console.ReadLine(), out value))
                {
                    ourConverter.convToMertes(value);
                    break;
                }
                else
                {
                    Console.WriteLine("Неправильный формат данных");
                }
            }
            Console.ReadLine();
        }     
    }
}
