﻿namespace LinqToXmlHomework
{
    /// <summary>
    /// Интерфейс для работы с файлами
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IRepository<T>:IGenerate<T>
        where T:class
    {
        string GetFileName();
        object Deserialize(string filePath);
        void Serialize(string filePath,object workObject);
    }
}
