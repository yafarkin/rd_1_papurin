﻿using System;
using System.Configuration;
namespace LinqToXmlHomework
{
    /// <summary>
    /// Класс для взаимодействия с
    /// кастомной секцией
    /// </summary>
    public class CustomSectionConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Block")]
        public BlockCollection BlockItems
        {
            get { return ((BlockCollection)(base["Block"])); }
        }
    }
}
