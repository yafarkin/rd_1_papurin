﻿using System.Configuration;

namespace LinqToXmlHomework
{
    /// <summary>
    /// Класс для взаимодействия непосредственно
    /// с необходимыми данными
    /// </summary>
    public class BlockElement : ConfigurationElement
    {

        [ConfigurationProperty("fileName", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string fileName
        {
            get { return ((string)(base["fileName"])); }
            set { base["fileName"] = value; }
        }
    }
}
