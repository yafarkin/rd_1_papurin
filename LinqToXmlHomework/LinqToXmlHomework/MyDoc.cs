﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LinqToXmlHomework
{
    /// <summary>
    /// Класс,реализующий интерфейсы IRepository и IGenerate
    /// Для файлов типа XML
    /// </summary>
    class MyXmlDoc : IRepository<XDocument>
    {
        /// <summary>
        /// Метод,реализующий создание каталога
        /// растений в виде Xml документа
        /// </summary>
        /// <returns></returns>
        public XDocument Create()
        {
            XDocument PlantsDoc = new XDocument();
            XElement Plants =
                new XElement("CATALOG",
                new XElement("PLANT",
                    new XElement("COMMON", "Cowslip",
                        new XAttribute("isPoison", true)),
                    new XElement("BOTANICAL", "Caltha palustris"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE", 9.90,
                        new XAttribute("dimension", "dollars")),
                    new XElement("AVAILABILITY", 10)),
                new XElement("PLANT",
                    new XElement("COMMON", "Dutchman's-Breeches",
                        new XAttribute("isPoison", false)),
                    new XElement("BOTANICAL", "Dicentra cucullaria"),
                    new XElement("ZONE", 3),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE", 6.44,
                        new XAttribute("dimension", "dollars")),
                    new XElement("AVAILABILITY", 234)),
                new XElement("PLANT",
                    new XElement("COMMON", "Ginger, Wild",
                        new XAttribute("isPoison", true)),
                    new XElement("BOTANICAL", "Asarum canadense"),
                    new XElement("ZONE", 3),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE", 9.03,
                        new XAttribute("dimension", "euros")),
                    new XElement("AVAILABILITY", 3)),
                new XElement("PLANT",
                    new XElement("COMMON", "Hepatica",
                        new XAttribute("isPoison", false)),
                    new XElement("BOTANICAL", "Hepatica americana"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE", 4.45,
                        new XAttribute("dimension", "dollars")),
                    new XElement("AVAILABILITY", 56)),
                new XElement("PLANT",
                    new XElement("COMMON", "Liverleaf",
                        new XAttribute("isPoison", false)),
                    new XElement("BOTANICAL", "Hepatica americana"),
                    new XElement("ZONE", 4),
                    new XElement("LIGHT", "Mostly Shady"),
                    new XElement("PRICE", 3.99,
                        new XAttribute("dimension", "euros")),
                    new XElement("AVAILABILITY", 37)));
            PlantsDoc.Add(Plants);
            return PlantsDoc;
        }
        /// <summary>
        /// Метод возвращает имя для итогового файла
        /// с деревом классов из кастомной секции в app.config
        /// </summary>
        /// <returns>string filePath</returns>
        public string GetFileName()
        {
            int j = 0;
            string filePath = string.Empty;
            CustomSectionConfigSection section = (CustomSectionConfigSection)ConfigurationManager.GetSection("CustomSection");
            if (section != null)
            {
                while (true)
                {
                    filePath = section.BlockItems[0].fileName + j + ".xml";
                    if (File.Exists(filePath))
                    {
                        j++;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return filePath;
        }
        /// <summary>
        /// Сохраняет документ с именем filePath
        /// в виде xml документа
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="item"></param>
        public void SavePlease(string filePath, XDocument item)
        {
            item.Save(filePath);
        }
        /// <summary>
        /// Метод сериализует объект workObject
        /// в Xml файл filePath
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="workObject"></param>
        public void Serialize(string filePath, object workObject)
        {

            XmlSerializer serialize = new XmlSerializer(typeof(Catalog));
            using (FileStream fileStreamSerialize = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                serialize.Serialize(fileStreamSerialize, workObject);
            }
        }
        /// <summary>
        /// Возвращает объект десериализованный
        /// из Xml файла filePath
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public object Deserialize(string filePath)
        {
            object workObject;
            XmlSerializer deSerialize = new XmlSerializer(typeof(Catalog));
            using (FileStream fileSreamDeserialize = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                workObject = deSerialize.Deserialize(fileSreamDeserialize);
            }
            return workObject;
        }
    }
}
