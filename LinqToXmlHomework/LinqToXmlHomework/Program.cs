﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Configuration;

namespace LinqToXmlHomework
{
    class Program
    {
        static void Main(string[] args)
        {
            MyXmlDoc dokForWorking = new MyXmlDoc();
            string fileForWork = "Plants1.xml";
            var newDocument = dokForWorking.Create();
            dokForWorking.SavePlease(fileForWork, newDocument);
            var fileName = dokForWorking.GetFileName();
            var objForSerialize = dokForWorking.Deserialize(fileForWork);
            dokForWorking.Serialize(fileName, objForSerialize);        
            Console.Read();
        }
    }
}
