﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace LinqToXmlHomework
{
    [Serializable]
    [XmlRoot("CATALOG")]
    public class Catalog
    {
        [XmlElement("PLANT")]
        public Plant[] plant { get; set; }
    }
    [Serializable]
    public class Plant
    {
        [XmlElement("COMMON")]
        public Common common { get; set; }
        [XmlElement("BOTANICAL")]
        public string Botanical { get; set; }
        [XmlElement("ZONE")]
        public int Zone { get; set; }
        [XmlElement("LIGHT")]
        public string Light { get; set; }
        [XmlElement("PRICE")]
        public Price price { get; set; }
        [XmlElement("AVAILABILITY")]
        public int Availability { get; set; }
    }
    [Serializable]
    public class Common
    {
        [XmlText()]
        public string commonType { get; set; }
        [XmlAttribute("isPoison")]
        public bool isPoison { get; set; }
    }
    [Serializable]
    public class Price
    {
        [XmlText()]
        public double price { get; set; }
        [XmlAttribute("dimension")]
        public string dimension { get; set; }

    }
}
