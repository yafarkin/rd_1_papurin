﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXmlHomework
{
    /// <summary>
    /// Интерфейс для генерирования
    /// файлов типа T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IGenerate<T>
    {
        T Create();
        void SavePlease(string filePath, T item);
    }
}
