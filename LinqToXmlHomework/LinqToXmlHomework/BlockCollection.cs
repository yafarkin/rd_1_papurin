﻿using System.Configuration;

namespace LinqToXmlHomework
{
    /// <summary>
    /// Класс для взаимодействия с коллекцией
    /// в кастомной секции
    /// </summary>
    [ConfigurationCollection(typeof(BlockElement))]
    public class BlockCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new BlockElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((BlockElement)(element)).fileName;
        }

        public BlockElement this[int idx]
        {
            get { return (BlockElement)BaseGet(idx); }
        }
    }
}
