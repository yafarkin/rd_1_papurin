﻿using System;

namespace NWS.DAL
{
    public class EmployeeDetails
    {
        public int EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int OrderID { get; set; }
        public DateTime OrderDate { get; set; }
    }
}
