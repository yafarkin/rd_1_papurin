﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Data;
using System;

namespace NWS.DAL
{
    public class EmployeeDB
    {
        private string connectionString;
        public EmployeeDB()
        {
            connectionString = WebConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;
        }
        public EmployeeDB(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<EmployeeDetails> GetEmployees1(string city,string country,int? pageSize,int? pageNumber)
        {
            SqlConnection connect = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("GetEmployees1", connect);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter sqlParamCountry = new SqlParameter();
            sqlParamCountry.ParameterName = "@Country";
            if (country == null)
            {
                sqlParamCountry.Value = DBNull.Value;
            }
            else
            {
                sqlParamCountry.SqlDbType = SqlDbType.NVarChar;
                sqlParamCountry.Value = country;
            }
            sqlParamCountry.Direction = ParameterDirection.Input;
            command.Parameters.Add(sqlParamCountry);

            SqlParameter sqlParamCity = new SqlParameter();
            sqlParamCity.ParameterName = "@City";
            if (city == null)
            {
                sqlParamCity.Value = DBNull.Value;
            }
            else
            {
                sqlParamCity.SqlDbType = SqlDbType.NVarChar;
                sqlParamCity.Value = city;
            }
            sqlParamCity.Direction = ParameterDirection.Input;
            command.Parameters.Add(sqlParamCity);

            SqlParameter sqlParamPageSize = new SqlParameter();
            sqlParamPageSize.ParameterName = "@pageSize";
            if (pageSize == null)
            {
                sqlParamPageSize.Value = DBNull.Value;
            }
            else
            {
                sqlParamPageSize.SqlDbType = SqlDbType.Int;
                sqlParamPageSize.Value = pageSize;
            }
            sqlParamPageSize.Direction = ParameterDirection.Input;
            command.Parameters.Add(sqlParamPageSize);

            SqlParameter sqlParamPageNumber = new SqlParameter();
            sqlParamPageNumber.ParameterName = "@pageNumber";
            if (pageNumber == null || pageNumber == 0)
            {
                sqlParamPageNumber.Value = DBNull.Value;
            }
            else
            {
                sqlParamPageNumber.SqlDbType = SqlDbType.Int;
                sqlParamPageNumber.Value = pageNumber;
            }
            sqlParamPageNumber.Direction = ParameterDirection.Input;
            command.Parameters.Add(sqlParamPageNumber);

            List<EmployeeDetails> employees = new List<EmployeeDetails>();

            try
            {
                connect.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeDetails emp = new EmployeeDetails();
                    emp.EmployeeID = (int)reader["empId"];
                    emp.FirstName = (string)reader["FirstName"];
                    emp.LastName = (string)reader["LastName"];
                    emp.BirthDate = (DateTime)reader["BirthDate"];
                    emp.OrderID = (int)reader["OrderID"];
                    emp.OrderDate = (DateTime)reader["OrderDate"];
                    emp.City = (string)reader["City"];
                    emp.Country = (string)reader["Country"];
                    employees.Add(emp);
                }
                reader.Close();
                return employees;
            }
            catch (SqlException)
            {
                // Замените эту ошибку чем-то более специфичным. 
                // Здесь также можно протоколировать ошибки
                throw new ApplicationException("Ошибка данных");
            }
            finally
            {
                connect.Close();
            }

        }
        public List<EmployeeDetails> GetEmployees()
        {
            SqlConnection connect = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("GetEmployees", connect);
            command.CommandType = CommandType.StoredProcedure;
           
            List<EmployeeDetails> employees = new List<EmployeeDetails>();

            try
            {
                connect.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    EmployeeDetails emp = new EmployeeDetails();
                    emp.EmployeeID = (int)reader["empId"];
                    emp.FirstName = (string)reader["FirstName"];
                    emp.LastName = (string)reader["LastName"];
                    emp.BirthDate = (DateTime)reader["BirthDate"];
                    emp.OrderID = (int)reader["OrderID"];
                    emp.OrderDate = (DateTime)reader["OrderDate"];
                    emp.City = (string)reader["City"];
                    emp.Country = (string)reader["Country"];
                    employees.Add(emp);
                }
                reader.Close();
                return employees;
            }
            catch (SqlException)
            {
                // Замените эту ошибку чем-то более специфичным. 
                // Здесь также можно протоколировать ошибки
                throw new ApplicationException("Ошибка данных");
            }
            finally
            {
                connect.Close();
            }

        }
    }
}
