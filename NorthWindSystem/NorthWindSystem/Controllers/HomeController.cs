﻿using NWS.DAL;
using System.Collections.Generic;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;

namespace NorthWindSystem.Controllers
{
    public class HomeController : Controller
    {
        private EmployeeDB _empDB = new EmployeeDB();
        // GET: Home
        public ViewResult Index()
        {
            return View();
        }
    }
}