﻿using NWS.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NorthWindSystem.Controllers
{
    public class ValuesController : ApiController
    {
        public EmployeeDB _empDB = new EmployeeDB();
        public IEnumerable<EmployeeDetails> GetEmpl()
        {
            var model = _empDB.GetEmployees();
            return model;
        }
        [HttpPost]
        public IEnumerable<EmployeeDetails> GetEmployeeWithFilter(EmployeeDetails empDtls)
        {
            var model = _empDB.GetEmployees1(empDtls.City, empDtls.Country,empDtls.OrderID,empDtls.EmployeeID);
            return model;
        }
    }
}
