﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RD_HW3_1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tester = { 1, 2, 3, 4, 5 };
            tester.ShowArr();
            tester = tester.Shuffle();
            tester.ShowArr();
            Console.ReadLine();
        }
    }
    public static class ArrExtension
    {
        public static void ShowArr(this int[]ourArray)
        {
            for (int i=0;i<ourArray.Length;i++)
            {
                Console.Write(ourArray[i]+" ");
            }
            Console.WriteLine(" ");
        }
        public static int[] Shuffle(this int[]ourArray)
        {
            Random randCounter = new Random();
            for (int i=ourArray.Length-1; i>0; i--)
            {
                int randNumber = randCounter.Next(i);
                var buffer = ourArray[i];
                ourArray[i] = ourArray[randNumber];
                ourArray[randNumber] = buffer;
            }
            return ourArray;
        }
    }
}
