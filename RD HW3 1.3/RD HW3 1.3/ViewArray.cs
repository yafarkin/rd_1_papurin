﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionMethod
{
    static class ViewArray
    {
        public static void ShowArray(string type,int kolvoStolbcov)
        {
            switch (type)
            {
                case "string":
                    {
                        string[] massiv = new string[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;
                    }
                case "char":
                    {
                        char[] massiv = new char[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;
                    }
                case "decimal":
                    {
                        decimal[] massiv = new decimal[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;
                    }
                case "int":
                    {
                        int[] massiv = new int[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;
                    }
                case "double":
                    {
                        double[] massiv = new double[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;
                    }
                case "float":
                    {
                        float[] massiv = new float[kolvoStolbcov];
                        for (int i = 0; i < kolvoStolbcov; i++)
                        {
                            Console.Write(" {0}", massiv[i]);
                        }
                        break;                        
                    }
                default:
                    {
                        Console.WriteLine("Тип данных не поддерживается");
                        break;
                    }
            }
        }
    } 
}
