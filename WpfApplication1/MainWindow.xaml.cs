﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string sumstr;
        /// <summary>
        /// Описание кнопок для основного окна
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Метод отрабатывает создание файла
        /// При нажатии кнопки
        /// </summary>
        /// <param name="CreateFile"></param>
        /// <param name="e"></param>
        private void Button_Click_Create(object CreateFile, RoutedEventArgs e)
        {
            string text = textBox.Text;
            var sumstr= string.Empty;
            if (!string.IsNullOrEmpty(text))
            {
                sumstr = text + ".txt";
                if (File.Exists(sumstr))
                {
                    MessageBox.Show("Файл уже существует,выберите другое имя");
                }
                else
                {
                    File.Create(sumstr);
                    MessageBox.Show("Документ создан");
                }
            }
            else
            {
                MessageBox.Show("Поле пусто,введите название файла");
            }
        }
        /// <summary>
        /// Метод отраатывает нажатие кнопки
        /// При котором форматируются данные из файла
        /// Имя которого пользователь
        /// Вводит в специальное поле
        /// </summary>
        /// <param name="FormatFile"></param>
        /// <param name="e"></param>
        private void Button_Click_Format(object FormatFile, RoutedEventArgs e)
        {
            string text = textBox1.Text;
            sumstr = text + ".txt";
            if (File.Exists(sumstr))
            {
                try
                {
                    string stroka = string.Empty;
                    using (StreamReader sr = new StreamReader(sumstr))
                    {
                            while ((stroka = sr.ReadLine()) != null)
                            {
                                string[] ololo = mkps(stroka);
                                listBox2.Items.Add(string.Format("X:{0}", ololo[0]));
                                listBox2.Items.Add(string.Format("Y:{0}", ololo[1]));
                            }
                    MessageBox.Show("Успешно отредактировано");
                    }
                }
                catch
                {
                }
            }
            else
            {
                MessageBox.Show("Файла не существует,ахтунг");
            }
        }
        /// <summary>
        /// Метод отраатывает нажатие кнопки
        /// При котором форматируются данные
        /// Которые пользователь
        /// Вводит в специальное поле
        /// </summary>
        /// <param name="FormatFile"></param>
        /// <param name="e"></param>
        private void Button_Click_DataInvite(object FormatFile, RoutedEventArgs e)
        {
            float[] f= new float[2];
            string[] masstr= new string[2];
            masstr[0] = textBox2.Text;
            masstr[1] = textBox3.Text;
            if ((float.TryParse(masstr[0],out f[0])) && (float.TryParse(masstr[1],out f[1])))
            {
                listBox1.Items.Add(string.Format("X:{0}",masstr[0]));
                listBox1.Items.Add(string.Format("Y:{0}",masstr[1]));
            }
            else
            {
                MessageBox.Show("Неправильный формат данных.Должны быть данные типа 33,44");
            }
        }
        /// <summary>
        /// Метод реализует форматирование данных
        /// </summary>
        /// <param name="sumstr"></param>
        /// <returns>String[] ss</returns>
        private string[] mkps(string sumstr)
        {
            string[] ss = sumstr.Split(',');
            for (int i=0; i<ss.Length; i++)
            {
             ss[i]  = ss[i].Replace('.', ',');
            }
            return ss;
        }
        /// <summary>
        /// Отрабатывает нажатие кнопки при 
        /// Котором вызывается метод для очистки списка
        /// </summary>
        /// <param name="FormatFile"></param>
        /// <param name="e"></param>
        private void Button_Click_Clear(object FormatFile, RoutedEventArgs e)
        {
            listBox1.Items.Clear();
        }
        /// <summary>
        /// Отрабатывает нажатие кнопки при 
        /// Котором вызывается метод для очистки списка
        /// </summary>
        /// <param name="FormatFile"></param>
        /// <param name="e"></param>
        private void Button_Click_Clear1(object FormatFile, RoutedEventArgs e)
        {
            listBox2.Items.Clear();
        }
    }
}
