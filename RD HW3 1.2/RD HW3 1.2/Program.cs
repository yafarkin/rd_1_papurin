﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RD_HW3_1._2
{
    class Program
    {        
        static void Main(string[] args)
        {
            decimal mon1;
            string money;
            bool flag=true;
            while (flag)
            {
                Console.WriteLine("На данный момент курс равен {0}", ConvertToRubl.dollarcourse);
                Console.WriteLine("Хотите поменять курс? 1-да 2-нет");
                switch (Console.ReadLine())
                {
                    case "1":
                        {
                            Console.WriteLine("Введите актуальный курс");
                            ConvertToRubl.dollarcourse = decimal.Parse(Console.ReadLine());
                            flag = false;
                            break;
                        }
                    case "2":
                        {
                            flag = false;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Неправильный формат ответа,введите 1 или 2");
                            break;
                        }
                }
            }
            while (true)
            {
                Console.WriteLine("Введите сумму денег для конвертации");
                money = Console.ReadLine();
                if (decimal.TryParse(money, out mon1))
                {
                    mon1 = ConvertToRubl.DollarToRubl(mon1);
                    Console.WriteLine("Сумма в рублях: {0}", mon1);
                    break;
                }
                else
                {
                    Console.WriteLine("Неправильный формат данных,введите данные вида 11,22");
                }
            }
            Console.ReadLine();
        }
    }
}