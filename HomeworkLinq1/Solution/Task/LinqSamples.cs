﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SampleSupport;
using Task;
using Task.Data;
using System;

// Version Mad01

namespace SampleQueries
{
    [Title("LINQ Module")]
    [Prefix("Linq")]
    public class LinqSamples : SampleHarness
    {
        private readonly DataSource dataSource = new DataSource();

        private bool ValidateZipCode(string val)
        {
            long number;
            return long.TryParse(val, out number);
        }

        private bool ValidatePhone(string phone)
        {
            var pattern = @"^(\([0-9]\))+?";
            return Regex.IsMatch(phone, pattern);
        }

        private List<GroupPriceEntity> SortProductsByPrice()
        {
            var sortedProducts = new List<GroupPriceEntity>();

            foreach (var prod in dataSource.Products)
                if (prod.UnitPrice <= 20M)
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 0 });
                else if ((prod.UnitPrice > 20M) && (prod.UnitPrice <= 50M))
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 1 });
                else
                    sortedProducts.Add(new GroupPriceEntity { product = prod, Group = 2 });

            return sortedProducts;
        }


        [Category("Restriction Operators")]
        [Title("Task sample")]
        [Description("Описание задачи на русском языке")
        ]
        public void LinqForExample()
        {
            var customers = dataSource
                .Customers
                .Select(c => c);

            //Он умеет выполнять запросы самостоятельно. Так же умеет делать foreach по коллекции. 
            //Проверьте это все запустив приложение и сделав запуск запроса
            ObjectDumper.Write(customers);
        }
        [Category("Restriction Operators")]
        [Title("Task 1")]
        [Description("Выборка клиентов с суммарным оборотом выше Х")
        ]
        public void Linq01()
        {
            var fringeSum = new[] { 22000, 26000, 30000 };
            foreach (var fringe in fringeSum)
            {
                var customers = dataSource
                    .Customers.Where(x => x.Orders.Sum(y => y.Total) > fringe).Select(g => new
                    {
                        customerId = g.CustomerID,
                        customerName = g.CompanyName,
                        customerCity = g.City,
                        totalSum = g.Orders.Sum(x => x.Total)
                    });
                ObjectDumper.Write(customers);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 2")]
        [Description("Группировка поставщиков в тех же городах,что и клиенты")
        ]
        public void Linq02()
        {
            var _resultCollections = dataSource.Customers.Join(dataSource.Suppliers,
                                     customer => customer.Country,
                                     supplier => supplier.Country,
                                     (customer, supplier) => new
                                     {
                                         customerName = customer.CompanyName,
                                         SupplierCity = supplier.City,
                                         CustomerCity = customer.City,
                                         SuppliersName = supplier.SupplierName
                                     })
                                     .Where(x => x.CustomerCity == x.SupplierCity);
            ObjectDumper.Write(_resultCollections);
        }
        [Category("Restriction Operators")]
        [Title("Task 3")]
        [Description("Выборка заказов,с суммой больше X")
        ]
        public void Linq03()
        {
            var fringeSum = new[] { 6000, 8000, 10000 };
            foreach (var fringe in fringeSum)
            {
                var _resultCollection = dataSource.Customers.Select(x => x.Orders.Where(y => y.Total >= fringe));
                ObjectDumper.Write("Сумма заказа больше " + fringe);
                foreach (var orders in _resultCollection)
                {
                    foreach (var order in orders)
                    {
                        ObjectDumper.Write(order);
                    }
                }
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 4")]
        [Description("Список клиентов с датой начала отношений")
        ]
        public void Linq04()
        {
            var _resultCollections = dataSource.Customers.SelectMany(u => u.Orders,
                (s, l) => new { customerName = s.CompanyName, firsOrderDate = l.OrderDate })
                .GroupBy(x => x.customerName)
                .Select(g => g.OrderBy(x => x.firsOrderDate)
                .First());
            ObjectDumper.Write(_resultCollections);

        }
        [Category("Restriction Operators")]
        [Title("Task 5")]
        [Description("Список клиентов отсортированный по году,месяцу и объему продаж")
        ]
        public void Linq05()
        {
            var _resultCollections = dataSource.Customers.SelectMany(u => u.Orders,
                (s, l) => new { customerName = s.CompanyName, dataTime = l.OrderDate, total = l.Total })
                .GroupBy(x => x.customerName).Select(g => g.OrderBy(x => x.dataTime.Year)
                                           .ThenBy(x => x.dataTime.Month)
                                           .ThenByDescending(x => x.total)
                                           .ThenBy(x => x.customerName));
            foreach (var group in _resultCollections)
                foreach (var customer in group)
                    ObjectDumper.Write(customer);
        }
        [Category("Restriction Operators")]
        [Title("Task 6")]
        [Description("Список клиентов,у которых нецифровой код,незаполнен регион,или не указан код оператора")
        ]
        public void Linq06()
        {
            var _resultCollections = dataSource.Customers.Select(x => new
            {
                customerName = x.CompanyName,
                phone = x.Phone,
                postCode = x.PostalCode,
                region = x.Region
            })
            .Where(x => x.region != null || !x.phone.StartsWith("(") || x.postCode.Contains(@"^1-9"));
            ObjectDumper.Write(_resultCollections);
        }
        [Category("Restriction Operators")]
        [Title("Task 7")]
        [Description("Группировка продуктов по категориям,внутри по наличию на складе,внутри последней группы по стоимости")
        ]
        public void Linq07()
        {
            var _resultCollections = dataSource.Products.GroupBy(x => x.Category)
            .Select(y => y.GroupBy(x => x.UnitsInStock)
            .Select(z => z.OrderBy(x => x.UnitPrice)));
            foreach (var p1 in _resultCollections)
            {
                ObjectDumper.Write("Группировка по категории");
                foreach (var p2 in p1)
                {
                    ObjectDumper.Write("Группировка по UnitInStock");
                    ObjectDumper.Write(p2);
                }
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 8")]
        [Description("Разбиение товаров на группы:дорогие,средние,дешевые")
        ]
        public void Linq08()
        {
            var _resultCollection = SortProductsByPrice().GroupBy(x => x.Group);
            foreach (var products in _resultCollection)
            {
                ObjectDumper.Write(products.Key);
                foreach (var p1 in products)
                {
                    ObjectDumper.Write(p1.product);
                }
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 9")]
        [Description("Средняя прибыльность каждого города")
        ]
        public void Linq09()
        {
            var _resultCollection = dataSource.Customers.GroupBy(x => x.City)
            .Select(x => new
            {
                city = x.Key,
                selector = x.Select(y => new
                {
                    clientName = y.CompanyName,
                    orderSum = y.Orders.Sum(z => z.Total),
                    totalOrders = y.Orders.Count()
                })
            })
            .Select(y => new
            {
                city = y.city,
                midOrderSum = (int)(y.selector.Sum(x => x.orderSum) / y.selector.Sum(z => z.totalOrders)),
                midOrdersForClient = (int)(y.selector.Sum(x => x.totalOrders) / y.selector.Count())
            });
            foreach (var customers in _resultCollection)
            {
                ObjectDumper.Write(customers);
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 10.1")]
        [Description("Активность клиента по месяцам")
        ]
        public void Linq10p1()
        {
            var _resultCollection = dataSource.Customers.SelectMany(g => g.Orders,
                    (s, m) => new
                    {
                        customerName = s.CompanyName,
                        customerCity = s.City,
                        orderData = m.OrderDate.Month,
                        orderTotal = m.Total
                    })
                    .GroupBy(g => g.customerName)
                    .Select(x => x.GroupBy(y => y.orderData))
                    .Select(x => x.Select(y => new
                    {
                        City = y.First().customerCity,
                        Client = y.First().customerName,
                        TotalSum = y.Sum(z => z.orderTotal),
                        Month = y.First().orderData
                    }));
            foreach (var customers in _resultCollection)
            {
                ObjectDumper.Write("Group by city : " + customers.First().City);
                foreach (var customer in customers)
                {
                    ObjectDumper.Write("Group by months : " + customer.Month);
                    ObjectDumper.Write(customer);
                }
            }
        }
        [Category("Restriction Operators")]
        [Title("Task 10.2")]
        [Description("Активность клиента по годам")
        ]
        public void Linq10p2()
        {
            var _resultCollection = dataSource.Customers.SelectMany(g => g.Orders,
                     (s, m) => new
                     {
                         customerName = s.CompanyName,
                         customerCity = s.City,
                         orderData = m.OrderDate.Year,
                         orderTotal = m.Total
                     })
                     .GroupBy(g => g.customerName)
                     .Select(x => x.GroupBy(y => y.orderData))
                     .Select(x => x.Select(y => new
                     {
                         City = y.First().customerCity,
                         Client = y.First().customerName,
                         TotalSum = y.Sum(z => z.orderTotal),
                         Year = y.First().orderData
                     }));
            foreach (var customers in _resultCollection)
            {
                ObjectDumper.Write("Group by city : " + customers.First().City);
                foreach (var customer in customers)
                {
                    ObjectDumper.Write("Group by year : " + customer.Year);
                    ObjectDumper.Write(customer);
                }
            }
        }

        [Category("Restriction Operators")]
        [Title("Task 10.3")]
        [Description("Активность клиента по месяцам и годам")
        ]
        public void Linq10p3()
        {
            var _resultCollection = dataSource.Customers.SelectMany(g => g.Orders,
                    (s, m) => new
                    {
                        customerName = s.CompanyName,
                        customerCity = s.City,
                        orderData = m.OrderDate,
                        orderTotal = m.Total
                    })
                    .GroupBy(g => g.customerName)
                    .Select(x => new
                    {
                        customerN = x.Key,
                        gr1 = x.GroupBy(y => y.orderData.Year)
                    })
                    .Select(x => x.gr1.Select(y => y.GroupBy(z => z.orderData.Month)));
            foreach (var customers in _resultCollection)
            {
                ObjectDumper.Write("Group by customerName");
                foreach (var customer in customers)
                {
                    ObjectDumper.Write("Group by year");
                    ObjectDumper.Write(customer);
                    foreach (var cust in customer)
                    {
                        ObjectDumper.Write("Group by month");
                        ObjectDumper.Write(cust);
                    }
                }
            }
        }
    }
}