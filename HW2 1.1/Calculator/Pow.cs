﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    /// <summary>
    /// Класс описывает методы вычисления
    /// корня n-ой степени
    /// </summary>
    public class Pow
    {
        /// <summary>
        /// Метод отрабатывает реализацию метода Ньютона
        /// Для нахождения корней n-ой степени
        /// </summary>
        /// <param name="number"></param>
        /// <param name="rate"></param>
        /// <param name="accuracy"></param>
        /// <returns></returns>
        public double Newtonsmethod(int number, double rate, int accuracy)
        {
            if (rate == 0)
            {
                return double.NaN;
            }
            double[] na = new double[accuracy + 1];
            na[0] = 1;
            for (int i = 1; i <= accuracy; i++)
            {
                na[i] = ((rate - 1) * na[i - 1]) / rate + (number / Math.Pow(na[i - 1], rate - 1)) / rate;
            }
            return na[accuracy];
        }
        /// <summary>
        /// Вызывает метод возведения в степень
        /// </summary>
        /// <param name="number"></param>
        /// <param name="rate"></param>
        /// <returns></returns>
        public double MP(int number, double rate)
        {
            return Math.Pow(number, rate);
        }
        /// <summary>
        /// Метод конвертирует string в int
        /// И выполняет необходимую валидацию
        /// </summary>
        /// <param name="numberForConvert"></param>
        /// <returns></returns>
        public int convertmethod(string numberForConvert)
        {
            int numberConvert = 0;
            if (int.TryParse(numberForConvert, out numberConvert))
            {
            }
            else
            {
                numberConvert = 0;
            }
            return numberConvert;
        }
    }
}
