﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Calculator;

namespace UI_HW2_1._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Инициализация основной формы
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// При нажатии отрабатывает вычисление корня по методу Ньютона
        /// При вводе пользователем данных в 3 отведённых поля
        /// </summary>
        /// <param name="CreateFile"></param>
        /// <param name="e"></param>
        private void Button_Click_NewtonsMethod(object CreateFile, RoutedEventArgs e)
        {
            Pow powNewton = new Pow();
            int inumber = powNewton.convertmethod(textBox.Text);
            double irate = powNewton.convertmethod(textBox1.Text);
            int iaccuracy = powNewton.convertmethod(textBox2.Text);
            if ((inumber != 0) && (irate != 0) && (iaccuracy != 0))
            {
                double result = powNewton.Newtonsmethod(inumber, irate, iaccuracy);
                listBox.Items.Add(Math.Round(result,2));
            }
            else MessageBox.Show("Неправильный формат данных,вводите числа больше 0");
        }
        /// <summary>
        /// Отрабатывает сравнение точности методов Math.Pow и Ньютона
        /// </summary>
        /// <param name="CreateFile"></param>
        /// <param name="e"></param>
        private void Button_Click_MethodsCompetition(object CreateFile, RoutedEventArgs e)
        {
            Pow competition = new Pow();
            int inumber = competition.convertmethod(textBox.Text);
            double irate = competition.convertmethod(textBox1.Text);
            int iaccuracy = competition.convertmethod(textBox2.Text);
            if ((inumber != 0) && (irate != 0) && (iaccuracy != 0))
            {
                double result = competition.Newtonsmethod(inumber, irate, iaccuracy);
                irate = 1 / irate;
                if (result != competition.MP(inumber, irate))
                {
                    if (result > competition.MP(inumber, irate))
                        MessageBox.Show("Метод Math.Pow точнее");
                    else
                        MessageBox.Show("Метод Newton с этой точностью лучше");
                }
                else
                    MessageBox.Show("Методы одинаковы по точности");
            }
            else MessageBox.Show("Неправильный формат данных,вводите числа больше 0");
        }
        /// <summary>
        /// Метод отрабатывает очистку списка при нажатии кнопки
        /// </summary>
        /// <param name="CreateFile"></param>
        /// <param name="e"></param>
        private void Button_Click_SectorClear(object CreateFile, RoutedEventArgs e)
        {
            listBox.Items.Clear();
        }
    }
}
