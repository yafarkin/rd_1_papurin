﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Converter
{
    /// <summary>
    /// Класс,содержащий методы конвертирования
    /// из десятичной системы,в двоичную
    /// </summary>
    public class Conv
    {
        /// <summary>
        /// Конвертирует в двоичный код из десятичного
        /// </summary>
        /// <param name="number">Число</param>
        /// <returns>Возвращает строку двоичного кода</returns>
        public string ConvToBinary(int number)
        {
            int ostatok;
            List<int> revbinarylist = new List<int>();
            while (number > 0)
            {
                ostatok = number % 2;
                number = number / 2;
                revbinarylist.Add(ostatok);
            }
            /*
            int[] binaryarrive = new int[revbinarylist.Count];
            
            for (int i = revbinarylist.Count - 1; i >= 0; i--)
            {
                binaryarrive[revbinarylist.Count - 1 - i] = revbinarylist[i];
            }
            */
            revbinarylist.Reverse();
            return string.Join("", revbinarylist);
        }
    }
}
