﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Converter;

namespace RD_HW_UI_2_1._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Класс основной формы
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Отрабатывает конвертирование числа,введённого в textBox 
        /// из десятичной системы в двоичную
        /// при нажатии кнопки
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Button_Click_Convert(object sender, EventArgs e)
        {
            int number;
            if (int.TryParse(textBox.Text, out number))
            {
                Conv NumberForConvert = new Conv();
                listBox.Items.Add(NumberForConvert.ConvToBinary(number));
            }
            else
                MessageBox.Show("Неправильный формат данных,введите целое число больше 0");
        }
        /// <summary>
        /// Handles the SectorClear event of the Button_Click control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Button_Click_SectorClear(object sender, EventArgs e)
        {
            listBox.Items.Clear();
        }
    }
}
