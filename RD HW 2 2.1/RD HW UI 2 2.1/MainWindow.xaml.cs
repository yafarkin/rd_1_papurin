﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PersonInfo;

namespace RD_HW_UI_2_2._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Инициализация основной формы
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Вызов свойства Address при нажатии кнопки
        /// при введении пользователем данных в textBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Button_Click_AddAddress(object sender, EventArgs e)
        {
            MailToData newadr = new MailToData();
            newadr.Address = textBox.Text;
            if (newadr.Address == "Error")
                MessageBox.Show("Неправильный формат данных.Введите данные типа- City,State,Zip");
            else
                listBox.Items.Add(newadr.Address);
        }
        /// <summary>
        /// Отрабатывает метод,очищающий listBox
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        public void Button_Click_SectorClear(object sender, EventArgs e)
        {
            listBox.Items.Clear();
        }
    }
}
