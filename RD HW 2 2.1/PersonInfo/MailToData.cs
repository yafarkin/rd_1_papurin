﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace PersonInfo
{
    /// <summary>
    /// Класс,описывающий фамилию и имя человека и его
    /// город,штат и индекс проживания
    /// </summary>
    public class MailToData
    {
        /// <summary>
        /// Возвращает не пустые поля City,State,Zip
        /// в виде одной строки
        /// </summary>
        public string Address
        {
            get
            {
                if ((ErrorFlagAddress == false) || (Regex.IsMatch(City,validation)) || (Regex.IsMatch(State, validation)) || (Regex.IsMatch(Zip, @"\W")))

                {
                    return "Error";
                }
                else
                    return string.Concat("City:",City,"\n", "State:", State, "\n", "Zip:",Zip);
            }
            set
            {
                splitFloor = value.Split(',');
                if (splitFloor.Length == 3)
                {
                    City = splitFloor[0];
                    State = splitFloor[1];
                    Zip = splitFloor[2];
                }
                else
                {
                    ErrorFlagAddress = false;
                }
            }
        }
        private string validation = @"\W\d";
        private string City, State, Zip,FirstName,SureName;
        /// <summary>
        /// Маркер для фиксации ошибок в свойстве Address
        /// </summary>
        public bool ErrorFlagAddress = true;
        /// <summary>
        /// Маркер для фиксации ошибок в свойстве Name
        /// </summary>
        public bool ErrorFlagName = true;
        string[] splitFloor;
        /// <summary>
        /// Возвращает Имя+Фамилия в строке
        /// </summary>
        /// <value>
        /// Фамилия и Имя
        /// </value>
        public string Name
        {
            get
            {
                if (ErrorFlagName == false)
                {
                    return "Error";
                }
                else
                    return string.Concat("FirstName:", FirstName, "\n", "SureName:", SureName);
            }
            set
            {
                splitFloor = value.Split(',');
                if (splitFloor.Length == 2)
                {
                    FirstName = splitFloor[0];
                    SureName = splitFloor[1];
                }
                else
                {
                    ErrorFlagName = false;
                }
            }
        }
    }
}
