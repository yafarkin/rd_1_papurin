﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Istor
{
    /// <summary>
    /// Интерфейс,описывающий сигнатуры методов для
    /// загрузки файла и записи в файл текста
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IStorage<T>
    {
        T LoadData(T path);
        void SaveData(T path,T Data); 
    }
}
