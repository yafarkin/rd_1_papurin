﻿using Istor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentWorker 
{
    /// <summary>
    /// Класс реализующий интерфейс IStorage
    /// </summary>
    /// <seealso />
    public class DocWorker : IStorage<string>
    {
        /// <summary>
        /// Метод,реализующий выгрузку текста из файла
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>Возвращает текст в виде строки</returns>
        public string LoadData(string path)
        {
            return File.ReadAllText(path);
        }
        /// <summary>
        /// Метод,реализующий запись текста data
        /// в файл,с путём path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="data">The data.</param>
        public void SaveData(string path, string data)
        {
            File.WriteAllText(path, data);
        }
    }
}
