﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DocumentWorker;
using System.IO;

namespace DocWorkerTest
{
    [TestClass]
    public class DWTest
    {
        [TestMethod]
        public void TestDocWorker()
        {
            //arrange
            string path = "testFile";
            string text = "Do you wand ice cream?";
            string result;
            //act
            DocWorker dw1 = new DocWorker();
            dw1.SaveData(path, text);
            result = dw1.LoadData(path);
            //assert
            var strContent = string.Empty;
            using (var file = new StreamReader("testFile"))
            strContent = file.ReadToEnd();
            Assert.AreEqual(strContent, "Do you wand ice cream?");
            File.Delete("testFile");
        }
    }
}
