﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DocumentWorker;

namespace RD_HW4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TextRedactor : Window
    {
        public TextRedactor()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Буфер для хранения пути к нужному файлу
        /// </summary>
        private string _currentFile;
        private string text;
        /// <summary>
        /// Метод реализует сохранение файла с заданным именем
        /// </summary>
        /// <returns></returns>
        public bool SaveDocumentAs()
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Text File(*txt)|*.txt";
            if (dlg.ShowDialog() == true)
            {
                _currentFile = dlg.FileName;
                return SaveDocument();
            }

            return false;
        }
        /// <summary>
        /// Метод реализует сохранение текущего файла
        /// </summary>
        /// <returns></returns>
        public bool SaveDocument()
        {
            if (string.IsNullOrEmpty(_currentFile))
                return SaveDocumentAs();
            else
            {
                {
                    DocWorker dw1 = new DocWorker();
                    text = new TextRange(bodyOfText.Document.ContentStart, bodyOfText.Document.ContentEnd).Text;
                    dw1.SaveData(_currentFile,text);
                }
                return true;
            }
        }
        /// <summary>
        /// Метод реализующий работу панели текстового редактора
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void menu_Click(object sender, RoutedEventArgs e)
        {
            
            MenuItem menu = e.OriginalSource as MenuItem;
            switch (menu.Name)
            {
                case "New":
                    MessageBoxResult messageBoxResult = MessageBox.Show("Документ будет потерян.Хотите сохранить?", "Сохранить", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        SaveDocument();
                    }
                    if (!string.IsNullOrEmpty(_currentFile))
                    {
                        _currentFile = null;
                    }
                    else
                    {
                        if (messageBoxResult == MessageBoxResult.No)
                            bodyOfText.Document.Blocks.Clear();
                    }
                    break;
                case "Open":
                    DocWorker dw1 = new DocWorker();
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.DefaultExt = ".txt";
                    dlg.Filter = "Text File(*txt)|*.txt";
                    if (dlg.ShowDialog() == true)
                    {
                        _currentFile = dlg.FileName;
                        TextRange range = new TextRange(bodyOfText.Document.ContentStart, bodyOfText.Document.ContentEnd);
                        range.Text = dw1.LoadData(_currentFile);
                    }
                    break;
                case "Save":
                    SaveDocument();
                    break;
                case "SaveAs":
                    SaveDocumentAs();          
                    break;
                case "Undo":
                    bodyOfText.Undo();
                    break;
                case "Redo":
                    bodyOfText.Redo();
                    break;
                case "Copy":
                    bodyOfText.Copy();
                    break;
                case "Cut":
                    bodyOfText.Cut();
                    break;
                case "Delete":
                    bodyOfText.Document.Blocks.Clear();
                    break;
            }
        }
    }
}
