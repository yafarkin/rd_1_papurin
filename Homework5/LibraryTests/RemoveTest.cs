﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework5;

namespace LibraryTests
{
    [TestClass]
    public class RemoveTest
    {
        Book testBook = new Book { id = 1, BookName = "Одиночество Мага", Author = "Ник Перумов" };
        [TestMethod]
        public void RemoveTester()
        {
            //arrange
            bool expected = true;
            bool actual;
            Library<Book> MyLibraryActual = new Library<Book>
                {
                new Book { id = 2, BookName = "Странствие Мага", Author = "Ник Перумов" },
                new Book { id = 1, BookName = "Одиночество Мага", Author = "Ник Перумов" },
                new Book { id = 3, BookName = "Прекрасный новый мир", Author = "Роман Злотников" },
            };
            Library<Book> MyLibraryExpected = new Library<Book>
                {
                new Book { id = 2, BookName = "Странствие Мага", Author = "Ник Перумов" },
                new Book { id = 3, BookName = "Прекрасный новый мир", Author = "Роман Злотников" },
            };
            //act
            MyLibraryActual.Remove(testBook);
            if (MyLibraryActual[0].Equals(MyLibraryActual[0], MyLibraryExpected[0]) && MyLibraryActual[2].Equals(MyLibraryActual[2], MyLibraryExpected[1]))
            {
                actual = true;
            }
            else
                actual = false;
            //assert
            Assert.AreEqual(actual, expected);
        }
    }
}
