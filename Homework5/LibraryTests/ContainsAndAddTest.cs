﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework5;

namespace LibraryTests
{
    [TestClass]
    public class ContainsAndAddTest
    {
        Book testBook = new Book { id = 1, BookName = "Одиночество Мага", Author = "Ник Перумов" };
        [TestMethod]
        public void ContainsAndAddTester()
        {
            //arrange
            bool expectedAdd = true;
            bool expectedCompare = true;
            Library<Book> MyLibraryActual = new Library<Book>();
            //act
            MyLibraryActual.Add(testBook);
            bool actualAdd = MyLibraryActual[0].Equals(MyLibraryActual[0], testBook);
            bool actualCompare = MyLibraryActual.Contains(testBook);
            //accert
            Assert.AreEqual(actualAdd, expectedAdd);
            Assert.AreEqual(actualCompare, expectedCompare);
        }
    }
}
