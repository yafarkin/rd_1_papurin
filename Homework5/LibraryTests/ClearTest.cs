﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework5;

namespace LibraryTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ClearTester()
        {
            //arrange
            bool actual;
            bool expected = true;
            Library<Book> MyLibraryActual = new Library<Book>
            {
                new Book { id = 1, BookName = "Одиночество Мага", Author = "Ник Перумов" }
                };
            //act
            MyLibraryActual.Clear();
            if (MyLibraryActual[0] == default(Book))
                actual = true;
            else
                actual = false;
            //accert
            Assert.AreEqual(actual, expected);
        }
    }
}
