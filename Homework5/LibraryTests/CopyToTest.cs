﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Homework5;

namespace LibraryTests
{
    [TestClass]
    public class CopyToTest
    {
        [TestMethod]
        public void CopyToTester()
        {
            bool actual = false;
            bool expected = true;
            //arrange
            Library<Book> MyLibraryActual = new Library<Book>
                {
                new Book { id = 2, BookName = "Странствие Мага", Author = "Ник Перумов" },
                new Book { id = 1, BookName = "Одиночество Мага", Author = "Ник Перумов" },
                new Book { id = 3, BookName = "Прекрасный новый мир", Author = "Роман Злотников" },
            };
            Book[] expectedArray = new Book[10];
            for (int i=0;i<expectedArray.Length;i++)
            {
                expectedArray[i] = default(Book);
            }
            //act
            MyLibraryActual.CopyTo(expectedArray, 5);
            for (int i=0;i < 3;i++)
                if (MyLibraryActual[i].Equals(MyLibraryActual[i],expectedArray[i+5]))
                {
                    actual = true;
                }
            else
                {
                    actual = false;
                    break;
                }
            //accert
            Assert.AreEqual(actual, expected);
        }
    }
}
