﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{

    /// <summary>
    /// Класс Библиотека для хранения
    /// объектов класса Т
    /// </summary>
    /// <typeparam name="T"></typeparam>

    public class Library<T>:ICollection<T>
    {

        /// <summary>
        /// Объявление библиотеки для хранения
        /// </summary>

        private T[] library;

        /// <summary>
        /// Операции  для передачи в делегат
        /// </summary>

        public enum OperationType { Added, Removed, Clear, Copy, Count, Contains, Get, Updated };
        /// <summary>
        /// Создание делегата OnChanger
        /// </summary>
        /// <param name="item"></param>
        /// <param name="operation"></param>

        public delegate void OnChange(T item, OperationType operation);
        OnChange changeViewer;

        /// <summary>
        /// Регистрирование делегата
        /// </summary>
        /// <param name="changeViewer1"></param>

        public void changeRegister(OnChange changeViewer)
        {
            this.changeViewer = changeViewer;
        }

        /// <summary>
        /// Поле,в котором хранится количество не пустых
        /// элементов коллекции
        /// </summary>

        private int count = 0;

        /// <summary>
        /// Реализация интерфейса IEnumerable<T>
        /// </summary>
        /// <returns></returns>

        IEnumerator IEnumerable.GetEnumerator()
        {
            return library.GetEnumerator();
        }
        public IEnumerator<T> GetEnumerator()
        {
            int i = 0;
            while (library.ElementAt(i) != null)
            {
                yield return library.ElementAt(i);
                i++;
            }
        }

        /// <summary>
        /// Инициализация экземпляра класса Библиотека
        /// </summary>

        public Library()
        {
            library = new T[count];
        }

        /// <summary>
        /// Доступ к коллекции по индексу
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>

        public T this[int index]
        {
            get
            {
                if (changeViewer != null)
                    changeViewer(library[index], OperationType.Get);
                return library[index];
            }
            set
            {
                if (changeViewer != null)
                    changeViewer(library[index], OperationType.Updated);
                library[index] = value;
            }
        }

        /// <summary>
        /// Метод,реализующий проверку коллекции
        /// на определённый объект класса T
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public bool Contains(T item)
        {
            bool found = false;
                if (library.Contains(item))
                {
                    found = true;
                }
            if (changeViewer != null)
                changeViewer(item, OperationType.Contains);
            return found;
        }

        /// <summary>
        /// Метод,реализующий добавление объекта T item
        /// в коллекцию
        /// </summary>
        /// <param name="item"></param>

        public void Add(T item)
        {
            if (!library.Contains(item))
            {
                if ((library.Length - Count) < 2)
                {
                    Array.Resize(ref library, library.Length + 10);
                    library[Count] = item;
                }
                else
                {
                    library[Count] = item;
                }
                if (changeViewer != null)
                    changeViewer(item, OperationType.Added);
                count++;
            }
            else
            {
                if (changeViewer != null)
                    changeViewer(item, OperationType.Contains);
            }
        }
       
        /// <summary>
        /// Метод,реализующий очистку коллекции
        /// и уменьшение её размера до 1 объекта
        /// </summary>

        public void Clear()
        {
            if (changeViewer != null)
                changeViewer(default(T), OperationType.Clear);
            Array.Clear(library, 0, library.Length);
            Array.Resize(ref library, 1);
            count = 0;
        }

        /// <summary>
        /// Метод,реализующий копирование объектов коллекции
        /// в массив,начиная с индекса ArrayIndex
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
                throw new ArgumentNullException("Список не должен быть пустым");
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("Начальный индекс массива не может быть отрицательным");
            }
            if (library.Length < array.Length-arrayIndex+1 )
                throw new ArgumentException("Массив имеет меньший размер,чем размер коллекции");
            for (int i = 0; i < arrayIndex; i++)
            {
                array[i+arrayIndex] = library[i];
            }
            if (changeViewer != null)
                changeViewer(default(T), OperationType.Copy);
        }
       
        /// <summary>
        /// Свойство для получаения кол-ва непустых элементов коллекции
        /// </summary>

        public int Count
        {
            get { return count; }
        }
        
        /// <summary>
        /// Свойство для доступа только для чтения
        /// </summary>

        public bool IsReadOnly
        {
            get { return false; }
        }

        /// <summary>
        /// Метод,реализующий удаление определённого объекта коллекции
        /// со сдвигом
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>

        public bool Remove(T item)
        {
            bool result = false;
            for (int i = 0; i < library.Count(); i++)
            {
                T curBook = (T)library[i];
                if (item.Equals(curBook))
                {
                    for (; i < library.Count() - 1; i++)
                    {
                        library[i] = library[i + 1];
                        library[i + 1] = default(T);
                    }
                    result = true;
                    count--;
                    if (changeViewer != null)
                        changeViewer(item, OperationType.Removed);
                    break;
                }
            }
            return result;
        }
    }
}
