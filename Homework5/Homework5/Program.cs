﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{
    class Program
    {
        static void Main(string[] args)
        {
            Library<Book> MyLibrary = new Library<Book>();
            Book test = new Book { id = 3, BookName = "Вьюга теней", Author = "Алексей Пехов" };
            Book test1 = new Book { id = 2, BookName = "Джанга с тенями", Author = "Алексей Пехов" };
            Book test2 = new Book { id = 1, BookName = "Танцующий с тенями", Author = "Алексей Пехов" };
            MyLibrary.Add(test);
            MyLibrary.Add(test1);
            MyLibrary.Add(test2);
            foreach (var i in MyLibrary)
            {
                Console.WriteLine(i.ToStr());
            }
            MyLibrary.Remove(test1);
            foreach (var i in MyLibrary)
            {
                Console.WriteLine(i.ToStr());
            }
            Console.ReadLine();
        }
    }
}
