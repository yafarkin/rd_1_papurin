﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{

    /// <summary>
    /// Класс для описания объекта Книга
    /// </summary>

    public class Book: IEqualityComparer<Book>
    {
        public int id;
        public string BookName, Author;

        /// <summary>
        /// Метод,реализующий сцепление трёх полей экземпляра класса в строку
        /// </summary>
        /// <returns></returns>

        public string ToStr()
        {
            return $"{id} {BookName} {Author}";
        }

        /// <summary>
        /// Метод,реализующий сравнение двух экземпляров
        /// класса по основным полям
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>

        public bool Equals(Book itemOne, Book itemTwo)
        {
            if ((itemOne.BookName == itemTwo.BookName) && (itemOne.Author == itemTwo.Author))
            {
                return true;
            }
            else
                return false;
        }

        public int GetHashCode(Book obj)
        {
            return GetHashCode();
        }
    }
}
