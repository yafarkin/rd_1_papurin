﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorksWithFile;
using WorkWithMatrix;

namespace RD_HW3_2._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void Button_ClickCreateMatrix(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            int introLine, introColumn;
            if ((int.TryParse(Line.Text, out introLine)) && (int.TryParse(Column.Text, out introColumn)))
            { 
                if ((introLine > 0) && (introColumn > 0))
                {
                    WWF FileCreate = new WWF();
                    if (Regex.IsMatch(ForCreateFile.Text, forValid))
                    {
                        FileCreate.CreateEmptyMatrix(ForCreateFile.Text, introLine, introColumn);
                    }
                    else
                    {
                        throw new Exception("Неправильный формат имени файла");
                    }
                }
                else
                {
                    throw new Exception("Неправильный формат введённых в поля данных");
                }
            }
            else
            {
                throw new Exception("Неправильный формат введённых в поля данных");
            }
        }
        private void Button_ClickOpenFile(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            WWF FileOpen = new WWF();
            int[,] OpenArray = FileOpen.OpenMatrix(ForOpenFile.Text);
            DataTable tableData = new DataTable();
            int rows = OpenArray.GetLength(0);
            int columns = OpenArray.GetLength(1);
            if (Regex.IsMatch(ForOpenFile.Text, forValid))
            {
                for (int i = 0; i < columns; i++)
                {
                    tableData.Columns.Add(new DataColumn(i.ToString()));
                }
                for (int j = 0; j < rows; j++)
                {
                    DataRow newRow = tableData.NewRow();
                    for (int k = 0; k < columns; k++)
                    {
                        int elementsOfArray = OpenArray[j, k];
                        newRow[k] = elementsOfArray;
                    }
                    tableData.Rows.Add(newRow);
                }
                dataGrid.ItemsSource = tableData.DefaultView;
            }
            else
            {
                throw new Exception("Неправильный формат имени файла");
            }
        }
        private void Button_ClickSaveChanges(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            int rows = dataGrid.Items.Count;
            int columns = dataGrid.Columns.Count;
            int[,] ourArray = new int[rows, columns];
            string FilePath = ForOpenFile.Text + ".txt";
            DataTable dt = new DataTable();
            DataView dv = (DataView)dataGrid.ItemsSource;
            dt = dv.Table;
            FileStream reWrite = new FileStream(FilePath, FileMode.Truncate);
            StreamWriter reWriter = new StreamWriter(reWrite);
            if (Regex.IsMatch(ForOpenFile.Text, forValid))
            {
                for (int i = 0; i < rows - 1; i++)
                {
                    DataRow newRow = dt.Rows[i];
                    for (int j = 0; j < columns; j++)
                    {
                        ourArray[i, j] = Convert.ToInt32(newRow[j]);
                        reWriter.Write(ourArray[i, j] + " ");
                    }
                    reWriter.WriteLine();
                }
                reWriter.Close();
            }
            else
            {
                throw new Exception("Неправильный формат имени файла");
            }
        }
        private void Button_ClickMatrixAddition(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            WWF FileWork = new WWF();
            int[,] MatrixOne = FileWork.OpenMatrix(MatrixForActions1.Text);
            int[,] MatrixTwo = FileWork.OpenMatrix(MatrixForActions2.Text);
            int rowsOne = MatrixOne.GetLength(0);
            int columnsOne = MatrixOne.GetLength(1);
            int rowsTwo = MatrixTwo.GetLength(0);
            int columnsTwo = MatrixTwo.GetLength(1);
            int[,] MatrixSummary = new int[rowsOne, columnsOne];
            if (Regex.IsMatch(MatrixResult.Text, forValid))
            {
                if ((rowsOne != rowsTwo) && (columnsOne != columnsTwo))
                {
                    throw new System.ArgumentException("Не совпадают размерности матриц");
                }
                else
                {
                    WWM matrixAd = new WWM();
                    MatrixSummary = matrixAd.MatrixAddition(MatrixOne, rowsOne, rowsTwo, MatrixTwo);
                }
                FileWork.SaveMatrix(MatrixResult.Text, MatrixSummary, rowsOne, columnsOne);
            }
            else
            {
                throw new Exception("Неправильный формат имени файла");
            }
        }
        private void Button_ClickMatrixMultiply(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            WWF FileWork = new WWF();
            int[,] MatrixOne = FileWork.OpenMatrix(MatrixForActions1.Text);
            int[,] MatrixTwo = FileWork.OpenMatrix(MatrixForActions2.Text);
            int rowsOne = MatrixOne.GetLength(0);
            int columnsOne = MatrixOne.GetLength(1);
            int rowsTwo = MatrixTwo.GetLength(0);
            int columnsTwo = MatrixTwo.GetLength(1);
            int[,] MatrixMul = new int[rowsOne, columnsTwo];
            if (Regex.IsMatch(MatrixMultiply.Text, forValid))
            {
                if (rowsOne != columnsTwo)
                {
                    throw new System.ArgumentException("Не совпадают размерности матриц");
                }
                else
                {
                    WWM matrixMu = new WWM();
                    MatrixMul = matrixMu.MatrixMultip(MatrixOne,rowsOne,columnsOne,MatrixTwo,rowsTwo,columnsTwo);
                }

                FileWork.SaveMatrix(MatrixMultiply.Text, MatrixMul, rowsOne, columnsOne);
            }
            else
            {
                throw new Exception("Неправильный формат имени файла");
            }
        }
        private void Button_ClickMatrixSubstraction(object sender, RoutedEventArgs e)
        {
            string forValid = @"[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]";
            WWF FileWork = new WWF();
            int[,] MatrixOne = FileWork.OpenMatrix(MatrixForActions1.Text);
            int[,] MatrixTwo = FileWork.OpenMatrix(MatrixForActions2.Text);
            int rowsOne = MatrixOne.GetLength(0);
            int columnsOne = MatrixOne.GetLength(1);
            int rowsTwo = MatrixTwo.GetLength(0);
            int columnsTwo = MatrixTwo.GetLength(1);
            int[,] MatrixSubtrac = new int[rowsOne, columnsOne];
            if (Regex.IsMatch(MatrixSubstaction.Text, forValid))
            {
                if ((rowsOne != rowsTwo) && (columnsOne != columnsTwo))
                {
                    throw new System.ArgumentException("Не совпадают размерности матриц");
                }
                else
                {
                    WWM matrixSu= new WWM();
                    MatrixSubtrac = matrixSu.MatrixSubsctr(MatrixOne,rowsOne,columnsOne, MatrixTwo);
                }

                FileWork.SaveMatrix(MatrixSubstaction.Text, MatrixSubtrac, rowsOne, columnsOne);
            }
            else
            {
                throw new Exception("Неправильный формат имени файла");
            }
        }
    }
}
