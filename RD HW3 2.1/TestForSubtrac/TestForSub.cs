﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkWithMatrix;

namespace TestForSubtrac
{
    [TestClass]
    public class TestForSub
    {
        [TestMethod]
        public void Subtraction()
            {
                //arrange
                int row = 4;
                int col = 4;
                int[,] ArrayA = {
                {1,1,1,1},
                {2,2,2,2},
                {3,3,3,3},
                {4,4,4,4},
            };
                int[,] ArrayB = {
                {4,4,4,4},
                {3,3,3,3},
                {2,2,2,2},
                {1,1,1,1},
            };
                int[,] ArrayExpected = {
                {-3,-3,-3,-3},
                {-1,-1,-1,-1},
                {1,1,1,1},
                {3,3,3,3},
            };
                //act
                WWM testWWM = new WWM();
                int[,] ArrayActual = testWWM.MatrixSubsctr(ArrayA, col, row, ArrayB);
                //assert
                CollectionAssert.AreEqual(ArrayExpected, ArrayActual);
            }
    }
}
