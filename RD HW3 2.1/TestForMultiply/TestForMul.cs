﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkWithMatrix;

namespace TestForMultiply
{
    [TestClass]
    public class TestForMul
    {
        [TestMethod]
        public void MultiplyTest()
        {
            //arrange
            int row = 4;
            int col = 4;
            int[,] ArrayA = {
                {1,1,1,1},
                {2,2,2,2},
                {3,3,3,3},
                {4,4,4,4},
            };
            int[,] ArrayB = {
                {4,4,4,4},
                {3,3,3,3},
                {2,2,2,2},
                {1,1,1,1},
            };
            int[,] ArrayExpected = {
                {10,10,10,10},
                {20,20,20,20},
                {30,30,30,30},
                {40,40,40,40},
            };
            //act
            WWM testWWM = new WWM();
            int[,] ArrayActual = testWWM.MatrixMultip(ArrayA, row, col, ArrayB, row, col);
            //assert
            CollectionAssert.AreEqual(ArrayExpected, ArrayActual);
        }
    }
}
