﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorksWithFile
{
    public class WWF
    {
        public void CreateEmptyMatrix(string FileName,int lineCount,int columnCount)
        {
            int[,] ourArray = new int[lineCount, columnCount];
            FileName = FileName + ".txt";
            FileStream FileForWrite = new FileStream(FileName,FileMode.Create);
            StreamWriter matrixWriter = new StreamWriter(FileForWrite);
            for (int i = 0; i < lineCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    ourArray[i, j] = 0;
                    matrixWriter.Write(ourArray[i, j] + " ");
                }
                matrixWriter.WriteLine();
            }
            matrixWriter.Close();
        }
        public int[,] OpenMatrix(string FileName)
        {
            FileName = FileName + ".txt";
            string[] strFile = File.ReadAllLines(FileName);
            List<int> ourList = new List<int>();
            string[] buffer;
            int buffer1;
            for (int i = 0; i < strFile.Length; i++)
            {
                buffer = strFile[i].Split(' ');
                for (int j=0;j<buffer.Length-1;j++)
                {
                    int.TryParse(buffer[j], out buffer1);
                    ourList.Add(buffer1);
                    /*ourArray[i] = new int[buffer.Length];
                    int.TryParse(buffer[j], out buffer1);
                    ourArray[i][j] = buffer1;*/
                }
                /*ourArray[i] = strFile[i].Split(default(string[]), StringSplitOptions.RemoveEmptyEntries).Select(x=>int.Parse(x)).ToArray<int>(); */
            }
            int columns = ourList.Count / strFile.Length;
            int rows = strFile.Length;
            int[,] ourArray = new int[rows, columns];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    ourArray[i, j] = ourList[i * columns + j];
                }
            }
            return ourArray;
        }
        public void DeleteFile(string FileName)
        {
            FileName = FileName + ".txt";
            File.Delete(FileName);
        }
        public void SaveMatrix(string FileName,int[,] ourArray, int lineCount, int columnCount)
        {
            int[,] newOurArray = new int[lineCount,columnCount];
            FileName = FileName + ".txt";
            File.WriteAllText(FileName, "");
            FileStream FileForWrite = new FileStream(FileName, FileMode.Create);
            StreamWriter matrixWriter = new StreamWriter(FileForWrite);
            for (int i = 0; i < lineCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    newOurArray[i,j] = ourArray[i, j];
                    matrixWriter.Write(ourArray[i, j] + " ");
                }
                matrixWriter.WriteLine();
            }
            matrixWriter.Close();
        }
    }
}
