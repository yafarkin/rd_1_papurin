﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkWithMatrix
{
    public class WWM
    {
        public int[,] MatrixAddition(int[,]ArrayNumA,int row,int col,int[,]ArrayNumB)
        {
            int[,] MatrixAdd = new int[row, col];
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < col; j++)
                    {
                    MatrixAdd[i, j] = ArrayNumA[i, j] + ArrayNumB[i, j];
                    }
                }
            return MatrixAdd;
        }
        public int[,] MatrixSubsctr(int[,] ArrayNumA, int row, int col, int[,] ArrayNumB)
        {
            int[,] MatrixSub = new int[row, col];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    MatrixSub[i, j] = ArrayNumA[i, j] - ArrayNumB[i, j];
                }
            }
            return MatrixSub;
        }
        public int[,] MatrixMultip(int[,] ArrayNumA, int rowOne, int colOne, int[,] ArrayNumB,int rowTwo,int colTwo)
        {
            int[,] MatrixMul = new int[rowOne, colTwo];
            for (int i = 0; i < rowOne; i++)
            {
                for (int j = 0; j < colTwo; j++)
                {
                    MatrixMul[i, j] = 0;
                    for (int k = 0; k < colOne; k++)
                    {
                        MatrixMul[i, j] += ArrayNumA[i, k] * ArrayNumB[k, j];
                    }
                }
            }
            return MatrixMul;
        }
    }
}
