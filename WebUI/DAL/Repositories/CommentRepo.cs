﻿using DAL.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL.Repositories
{
    /// <summary>
    /// Класс,реализующий работу с даными
    /// </summary>
    public class CommentRepo : ICommentRepo
    {
        /// <summary>
        /// Метод,реализующий добавление комментария в базу
        /// </summary>
        /// <param name="item"></param>
        public void Add(CommentariesDto item)
        {
            Commentaries selectedComment = new Commentaries
            {
                Text = item.Text,
                CommentaryDate = DateTime.Now,
                NewsId = item.NewsId
            };
            using (var DbContext = new NewsEntities())
            {
                DbContext.Commentaries.Add(selectedComment);
                DbContext.SaveChanges();
            }
        }
        /// <summary>
        /// Метод,реализующий удаление комментария из базы
        /// </summary>
        /// <param name="item"></param>
        public void Delete(CommentariesDto item)
        {
            Commentaries selectedComment = new Commentaries();
            selectedComment.Text = item.Text;
            selectedComment.CommentaryDate = item.CommentaryDate;
            selectedComment.Id = item.Id;
            selectedComment.NewsId = item.NewsId;
            using (var DbContext = new NewsEntities())
            {
                DbContext.Commentaries.Remove(selectedComment);
                DbContext.SaveChanges();
            }
        }
        /// <summary>
        /// Метод,реализующий поиск комментария по его тексту
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IEnumerable<CommentariesDto> Find(CommentariesDto item)
        {
            IEnumerable<CommentariesDto> returnedData;
            using (var DbContext = new NewsEntities())
            {
                returnedData = DbContext.Commentaries.Where(y => y.Text == item.Text)
                .Select(x => new CommentariesDto
                {
                    Id = x.Id,
                    Text = x.Text,
                    CommentaryDate = x.CommentaryDate,
                    NewsId = x.NewsId
                }).ToList();
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий получение всех комментариев
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CommentariesDto> GetAll()
        {
            IEnumerable<CommentariesDto> returnedData;
            using (var DbContext = new NewsEntities())
            {
                returnedData = DbContext.Commentaries.Select(x => new CommentariesDto
                {
                    CommentaryDate = x.CommentaryDate,
                    Text = x.Text,
                    Id = x.Id,
                    NewsId = x.NewsId
                }).ToList();
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий получение комментария по его Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CommentariesDto GetComment(int Id)
        {
            CommentariesDto returnedData;
            using (var DbContext = new NewsEntities())
            {
                var mappedData = DbContext.Commentaries.FirstOrDefault(x => x.Id == Id);
                returnedData = new CommentariesDto
                {
                    Id = mappedData.Id,
                    CommentaryDate = mappedData.CommentaryDate,
                    NewsId = mappedData.NewsId,
                    Text = mappedData.Text
                };
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий сохранение изменений
        /// </summary>
        public void Save()
        {
            using (var DbContext = new NewsEntities())
            {
                DbContext.SaveChanges();
            }
        }
        /// <summary>
        /// Метод,реализующий обновление отредактированного комментария
        /// </summary>
        /// <param name="item"></param>
        public void Update(CommentariesDto item)
        {            
            using (var DbContext = new NewsEntities())
            {
                var comment = DbContext.Commentaries.FirstOrDefault(x => x.Id == item.Id);
                comment.Id = item.Id;
                comment.CommentaryDate = item.CommentaryDate;
                comment.NewsId = item.NewsId;
                comment.Text = item.Text;
                DbContext.SaveChanges();
            }
        }
    }
}
