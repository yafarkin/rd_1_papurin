﻿using DAL.Interfaces;
using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Класс,позволяющий осуществлять работу 
    /// с сущностью Новости в базе данных
    /// </summary>
    public class NewsRepo : INewsRepo
    {
        /// <summary>
        /// Метод,реализующий добавление новости в базу
        /// </summary>
        /// <param name="item"></param>
        public void Add(NewsDto item)
        {
            using (var DbContext = new NewsEntities())
            {
                News selectedNews = new News
                {
                    Name = item.Name,
                    Text = item.Text,
                    Date = DateTime.Now,
                    ShortText = item.ShortText,
                };
                DbContext.News.Add(selectedNews);
                DbContext.SaveChanges();
            }
        }
        /// <summary>
        /// Метод,реализующий удаление новости из базы,вместе со связанными комментариями
        /// </summary>
        /// <param name="Id"></param>
        public void Delete(int Id)
        {
            using (var DbContext = new NewsEntities())
            {
                var comments = DbContext.Commentaries.RemoveRange(DbContext.Commentaries.Where(x => x.NewsId == Id));
                var news = DbContext.News.Find(Id);
                if (news != null)
                {
                    DbContext.News.Remove(news);
                    DbContext.SaveChanges();
                }
            }            
        }
        /// <summary>
        /// Метод,реализующий поиск и получение новости,по её тексту
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IEnumerable<NewsDto> Find(NewsDto item)
        {
            IEnumerable<NewsDto> returnedData;
            using (var DbContext = new NewsEntities())
            {
            returnedData = DbContext.News.Where(x => x.Text == item.Text)
            .Select(y => new NewsDto
            {
                Id = y.Id,
                Name = y.Name,
                Date = y.Date,
                Text = y.Text,
                ShortText = y.ShortText,
                CommentariesDto = y.Commentaries.Select(x => new CommentariesDto
                {
                    Id = x.Id,
                    Text = x.Text,
                    CommentaryDate = x.CommentaryDate,
                    NewsId = x.NewsId
                })
            }).ToList();
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий получение полного перечня новостей
        /// </summary>
        /// <returns></returns>
        public IEnumerable<NewsDto> GetAll()
        {
            IEnumerable<NewsDto> returnedData;
            using (var DbContext = new NewsEntities())
            {
                returnedData = DbContext.News.Select(x => new NewsDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Text = x.Text,
                    ShortText = x.ShortText,
                    Date = x.Date,
                    CommentariesDto = x.Commentaries.Select(y => new CommentariesDto
                    {
                        Id = y.Id,
                        Text = y.Text,
                        CommentaryDate = y.CommentaryDate,
                        NewsId = y.NewsId
                    })
                }).ToList();
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий получение новости,по её Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public NewsDto GetNews(int Id)
        {
            NewsDto mappedData;
            using (var DbContext = new NewsEntities())
            {
                mappedData = DbContext.News.Select(x => new NewsDto
                {
                    Name = x.Name,
                    Id = x.Id,
                    Date = x.Date,
                    ShortText = x.ShortText,
                    Text = x.Text,
                    CommentariesDto = x.Commentaries.Select(y => new CommentariesDto
                    {
                        Id = y.Id,
                        Text = y.Text,
                        CommentaryDate = y.CommentaryDate,
                        NewsId = y.NewsId
                    })
                }).Where(z => z.Id == Id).ToList().FirstOrDefault();
            }
            return mappedData;
        }
        /// <summary>
        /// Метод,реализующий получение страницы новостей,в зависимости от номера страницы и её размерности
        /// </summary>
        /// <param name="PageSize"></param>
        /// <param name="PageNumber"></param>
        /// <returns></returns>
        public IEnumerable<NewsDto> GetPage(int PageSize, int PageNumber)
        {
            IEnumerable<NewsDto> returnedData;
            using (var DbContext = new NewsEntities())
            {
                returnedData = DbContext.News.Where(x => (x.Id < (PageSize * PageNumber) && (x.Id >= PageSize * (PageNumber - 1)))).ToList()
                .Select(x => new NewsDto
                {
                    Date = x.Date,
                    Text = x.Text,
                    ShortText = x.ShortText,
                    Id = x.Id,
                    Name = x.Name,
                    CommentariesDto = x.Commentaries.Select(y => new CommentariesDto
                    {
                        Id = y.Id,
                        Text = y.Text,
                        CommentaryDate = y.CommentaryDate,
                        NewsId = y.NewsId
                    })
                });
            }
            return returnedData;
        }
        /// <summary>
        /// Метод,реализующий сохранение изменений в базе
        /// </summary>
        public void Save()
        {
            using (var DbContext = new NewsEntities())
            {
                DbContext.SaveChanges();
            }
        }
        /// <summary>
        /// Метод,реализующий обновление отредактированной новости
        /// </summary>
        /// <param name="item"></param>
        public void Update(NewsDto item)
        {
            using (var DbContext = new NewsEntities())
            {
                var news = DbContext.News.FirstOrDefault(x => x.Id == item.Id);
                news.Id = item.Id;
                news.Date = item.Date;
                news.ShortText = item.ShortText;
                news.Text = item.Text;
                news.Name = item.Name;
                DbContext.SaveChanges();
            }
        }
    }
}
