﻿using Model;
using System;
using System.Collections.Generic;

namespace DAL.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с новостями
    /// </summary>
    public interface INewsRepo
    {
        void Add(NewsDto item);
        void Delete(int Id);
        NewsDto GetNews(int Id);
        IEnumerable<NewsDto> GetPage(int PageSize,int PageNumber);
        void Update(NewsDto item);
        IEnumerable<NewsDto> Find(NewsDto item);
        void Save();
        IEnumerable<NewsDto> GetAll();
    }
}
