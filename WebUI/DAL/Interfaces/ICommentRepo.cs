﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с комментариями
    /// </summary>
    public interface ICommentRepo
    {
        void Add(CommentariesDto item);
        void Delete(CommentariesDto item);
        CommentariesDto GetComment(int Id);
        IEnumerable<CommentariesDto> GetAll();
        void Update(CommentariesDto item);
        IEnumerable<CommentariesDto> Find(CommentariesDto item);
        void Save();
    }
}
