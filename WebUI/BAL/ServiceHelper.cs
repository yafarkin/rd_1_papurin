﻿using BAL.Interfaces;
using BAL.Services;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.Practices.Unity;

namespace BAL
{
    public static class ServiceHelper
    {
        private static IUnityContainer _container;

        static ServiceHelper()
        {
            _container = new UnityContainer();
            _container.RegisterType<INewsService, NewsService>();
            _container.RegisterType<INewsRepo, NewsRepo>();
            _container.RegisterType<ICommentRepo, CommentRepo>();
            _container.RegisterType<ICommentService, CommentService>();
        }
        public static INewsService GetNewsService()
        {
            return _container.Resolve<INewsService>();
        }
        public static ICommentService GetCommentService()
        {
            return _container.Resolve<ICommentService>();
        }
    }
}
