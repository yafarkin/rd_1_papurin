﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Interfaces
{
    public interface ICommentService
    {
        void Add(CommentariesDto item);
        void Delete(CommentariesDto item);
        IEnumerable<CommentariesDto> Find(CommentariesDto item);
        IEnumerable<CommentariesDto> GetAll();
        CommentariesDto GetComment(int Id);
        void Save();
        void Update(CommentariesDto item);
    }
}
