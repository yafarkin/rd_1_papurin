﻿using Model;
using System.Collections.Generic;

namespace BAL
{
    public interface INewsService
    {
        IEnumerable<NewsDto> GetAll();
        void Add(NewsDto item);
        void Delete(int Id);
        NewsDto GetNews(int Id);
        void Save();
        void Update(NewsDto item);
    }
}
