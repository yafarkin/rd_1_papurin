﻿using BAL.Interfaces;
using System;
using System.Collections.Generic;
using Model;
using DAL.Interfaces;
using System.Linq;

namespace BAL.Services
{
    public class CommentService : ICommentService
    {
        private ICommentRepo _commentRepo;
        public CommentService(ICommentRepo commentRepo)
        {
            _commentRepo = commentRepo;
        }
        public void Add(CommentariesDto item)
        {
            _commentRepo.Add(item);
        }

        public void Delete(CommentariesDto item)
        {
            _commentRepo.Delete(item);
        }

        public IEnumerable<CommentariesDto> Find(CommentariesDto item)
        {
            return _commentRepo.Find(item);
        }

        public IEnumerable<CommentariesDto> GetAll()
        {
            var data = _commentRepo.GetAll().OrderByDescending(x => x.CommentaryDate);
            return data;
        }

        public CommentariesDto GetComment(int Id)
        {
            return _commentRepo.GetComment(Id);
        }

        public void Save()
        {
            _commentRepo.Save();
        }

        public void Update(CommentariesDto item)
        {
            _commentRepo.Update(item);
        }
    }
}
