﻿using DAL.Interfaces;
using System.Collections.Generic;
using Model;
using System.Linq;

namespace BAL
{
    public class NewsService : INewsService
    {
        private INewsRepo _newsRepo;
        public NewsService(INewsRepo newsRepo)
        {
            _newsRepo = newsRepo;
        }

        public void Add(NewsDto item)
        {
            _newsRepo.Add(item);
        }

        public void Delete(int Id)
        {
            _newsRepo.Delete(Id);
        }

        public IEnumerable<NewsDto> GetAll()
        {
            var data = _newsRepo.GetAll().OrderByDescending(x => x.Date);
            return data;
        }

        public NewsDto GetNews(int Id)
        {
            return _newsRepo.GetNews(Id);
        }

        public void Save()
        {
            _newsRepo.Save();
        }

        public void Update(NewsDto item)
        {
            _newsRepo.Update(item);
        }
    }
}
