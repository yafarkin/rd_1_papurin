﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Model;
using System;
using DAL;
using DAL.Repositories;
using System.Linq;

namespace Tests
{
    [TestClass]
    public class DALTests
    {
        [TestMethod]
        public void TestCreateMethod()
        {
            //arrange
            NewsRepo newsRepo = new NewsRepo();
            NewsDto new1 = new NewsDto
            {
                Name = "Имя новости",
                Date = DateTime.Today,
                ShortText = "Короткое представление новости",
                Text = "Текст новости",
                CommentariesDto = null
            };
            var expectedName = "Имя новости";
            var expectedShortText = "Короткое представление новости";
            var expectedText = "Текст новости";


            //act
            newsRepo.Add(new1);
            newsRepo.Save();

            var result = newsRepo.Find(new1).FirstOrDefault();

            // assert
            Assert.AreEqual(expectedName, result.Name);
            Assert.AreEqual(expectedShortText, result.ShortText);
            Assert.AreEqual(expectedText, result.Text);
        }
        [TestMethod]
        public void FindAndDeleteTest()
        {
            //arrange
            NewsRepo newsRepo = new NewsRepo();
            NewsDto new1 = new NewsDto
            {
                Name = "Имя новости",
                Date = DateTime.Today,
                ShortText = "Короткое представление новости",
                Text = "Текст новости",
                CommentariesDto = null
            };
            //act   
            var news = newsRepo.Find(new1).FirstOrDefault();
            if (news != null)
            {
                newsRepo.Delete(news.Id);
                newsRepo.Save();
            }
            news = newsRepo.Find(new1).FirstOrDefault();
            // assert
            Assert.AreEqual(null, news);
        }
    }
}
