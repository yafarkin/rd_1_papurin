﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class NewsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public DateTime? Date { get; set; }
        public string ShortText { get; set; }
        public IEnumerable<CommentariesDto> CommentariesDto { get; set; }
    }
}
