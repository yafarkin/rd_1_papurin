﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class CommentariesDto
    {
        public int Id { get; set; }
        public int? NewsId { get; set; }
        public string Text { get; set; }
        public DateTime? CommentaryDate { get; set; }
    }
}
