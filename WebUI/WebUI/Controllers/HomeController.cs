﻿using BAL;
using Model;
using System.Web.Mvc;
using PagedList;
using BAL.Interfaces;

namespace WebUI.Controllers
{
    public class HomeController : Controller
    {
        private ICommentService commentService;
        private INewsService newsService;
        public HomeController(ICommentService commentService,INewsService newsService)
        {
            this.commentService = commentService;
            this.newsService = newsService;
        }
        // GET: Home
        public ActionResult Index(int? page)
        {
            var data = newsService.GetAll();
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            ViewBag.NewsDto = data;
            return View(data.ToPagedList(pageNumber,pageSize));
        }
        [HttpGet]
        public ActionResult Delete(int Id)
        {
            newsService.Delete(Id);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(NewsDto model)
        {
            newsService.Add(model);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult CreateComment()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateComment(CommentariesDto model)
        {
            commentService.Add(model);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult MoreInformation(int Id)
        {
            var item = newsService.GetNews(Id);
            return View(item);
        }
    }
}