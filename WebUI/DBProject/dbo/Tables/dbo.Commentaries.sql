﻿CREATE TABLE [dbo].[Commentaries] (
    [Id]             INT IDENTITY (1,1)          NOT NULL,
    [NewsId]         INT           NULL,
    [CommentaryDate] DATETIME      NULL,
    [Text] NVARCHAR(MAX) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Commentaries_News] FOREIGN KEY ([NewsId]) REFERENCES [dbo].[News] ([Id])
);

